# php-laravel

## Finalidad
El proyecto debera recibir desde un ambiente externo nuevas Ventas ( producto, cantidad , valor , fecha ).
Debera poder ejecutarse diariamente un comando que genere una agrupación de las ventas por fecha y producto, teniendo como resultado la sumatoria final de valor y cantidad.
Finalmente, tendra un acceso a un panel donde se visualizará dicha información. Puede ser una tabla como un gráfico.
## Requerimentos 
* Crear un proyecto en Laravel Version >= 5.4

## Modelos
Generar los modelos usando el ORM Eloquent con sus respectiva identidad en la Base de Datos (MYSQL).
Es de gran utilidad disponer de los respectivos archivos migrations.
Basta tener el Modelo de una Venta y el Sumarizado por Dia  para tener una solucion, pero no quita la utilización de otros modelos si asi lo crea conveniente.

## Webservices
Publicar un endpoint (REST) donde se puedan recibir nuevas Ventas.

## Comandos
El comando debera correrse con "php artisan" y recibira como parametro la fecha que desea procesar.
Realizara la agrupación necesaria para proveer a la vista de datos.

## Web
Con una vista alcanza para mostrar los resultados.


